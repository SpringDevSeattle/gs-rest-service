package hello;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

@Configuration
public class TestConfig {


    @Bean
    public TestEnv testEnv(){
        Properties properties = System.getProperties();
        String env = properties.getProperty("app.env");
        System.out.println(env);
        if (env==null){
            env="dev";
        }
        return new TestEnv(env);
    }
}
